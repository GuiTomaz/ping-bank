import './Moeda.css'
import React, { useEffect, useState } from 'react'
import Header from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
import { obtem_cotacao } from '../../services/moedaAPI'

import SimpleDateTime from 'react-simple-timestamp-to-date'

function Moeda() {
    const [cotacao, setCotacao] = useState({})

    useEffect(() => {
        obtem_cotacao(setCotacao)
    }, [])
    //opcional
    useEffect(() => {
        console.log(cotacao)
    }, [cotacao])
    return (
        <>
            <Header></Header>
            <main>
                {/* <h1>{`COTACAO DO ${cotacao[0] && cotacao[0].name}`}</h1>
                <h1>{`COTACAO DO ${cotacao[2] && cotacao[2].name}`}</h1> */}
                <div className="cotacao">
                    <h1>COTAÇÃO DAS MOEDAS  </h1>
                    {cotacao.length > 0 ?
                        cotacao.map(cotacao => {
                            return (
                                <div className="card">
                                    <div className="moeda-nome">
                                        <h4>{cotacao.name}</h4>
                                        <h1>{cotacao.code}</h1>
                                    </div>
                                    <div className="info">
                                        <h2>Máxima: R$ {cotacao.high}</h2>
                                        <h2>Mínima: R$ {cotacao.low}</h2>
                                        <p>Atualizado em: <SimpleDateTime dateSeparator="/" format="DMY" timeSeparator=":" meridians="1">{cotacao.timestamp}</SimpleDateTime></p>
                                    </div>

                                </div>)
                        })
                        :
                        <p>Carregando...</p>}
                </div>
                <div className="gradiente">
                    
                </div>
            </main>
            <Footer></Footer>
        </>
    )
}

export default Moeda