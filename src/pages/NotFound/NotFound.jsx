import './NotFound.css'
import React from 'react'

import Header from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'

function NotFound() {
    return (
        <>
            <Header></Header>
            <main>
                <h1>Erro 404</h1>
            </main>
            <Footer></Footer>
        </>
    )
}

export default NotFound