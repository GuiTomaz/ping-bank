import './Inicio.css'
import React from 'react'
import Header from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
function Inicio() {
    return (
        <>
            <Header></Header>

            <main>
                <h1>Inicio pingbank</h1>
            </main>

            <Footer></Footer>
        </>

    )

}

export default Inicio