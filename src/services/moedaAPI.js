import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"
})

async function obtem_cotacao(setCotacao) {
    await api.get("/json/all")
        .then(response => {
            let listmoedas = []
            Object.keys(response.data).forEach(key => {
                listmoedas.push(response.data[key])
            })
            setCotacao(listmoedas)
        }).catch(error => {
            console.log(error)
        })
}

export { obtem_cotacao }