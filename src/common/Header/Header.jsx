import React from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router-dom'
import './Header.css'


function Header() {
    return (
        <>
            <header>
                <nav>
                    {/* <Link to="/emconstrucao">Metas</Link> */}
                    <div class="menu">
                        <div class="icon_menu">
                            <input type="checkbox" id="nav-menu2"></input>
                            <label for="nav-menu2">
                                <div id="nav-icon2">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </label>
                        </div>
                        <div class="hidden-menu">
                            <ul>
                                <li id="home"><Link to="/">Inicio</Link></li>
                                <li id="cot_moeda"><Link to="/moeda">Moeda</Link></li>
                            </ul>

                        </div>
                    </div>
                </nav>
            </header>
        </>
    )
}

export default Header