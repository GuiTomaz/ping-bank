import React from 'react'
import Moeda from './pages/Moeda/Moeda'
import EmConstrucao from './pages/EmConstrucao/EmConstrucao'
import NotFound from './pages/NotFound/NotFound'
import Inicio from './pages/Inicio/Inicio'
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'

function Routes() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/moeda">
                    <Moeda />
                </Route>
                <Route exact path="/emconstrucao">
                    <EmConstrucao />
                </Route>
                <Route exact path="/">
                    <Inicio />
                </Route>
                <Route path="">
                    <NotFound></NotFound>
                </Route>
            </Switch>
        </BrowserRouter>
    )
}

export default Routes